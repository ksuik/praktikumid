package praktikum6;

import praktikum1.TextIO;

public class M2ng {

	public static void main(String[] args) {

		int arvutiArv = suvalineArv(1,100) ;
		System.out.println("Arva ära number ");
		
		
		while (true){
			int kasutajaArvas = TextIO.getlnInt(); 
			if (arvutiArv == kasutajaArvas){
				System.out.println("Arvasite ära");
				break;
			} else if (arvutiArv > kasutajaArvas){
				System.out.println("See arv on suurem");
			} else {
				System.out.println("See arv on väiksem");
			}
		
		}	

	}

	public static int suvalineArv(int min, int max){
		int vahemik = max - min + 1;
		
		return (int) (Math.random() * vahemik) + min; 
	
	}
}
