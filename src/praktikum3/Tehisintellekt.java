package praktikum3;

import praktikum1.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {

		System.out.println("Sisesta kaks vanust");
		int esimeneVanus = TextIO.getlnInt();
		int teineVanus = TextIO.getlnInt();

			int vanusevahe = Math.abs(esimeneVanus - teineVanus);

			if (vanusevahe > 10) {
				System.out.println("Ei sobi!");

			} else if (vanusevahe > 5) {
				System.out.println("Hell no!");

			} else {
				System.out.println("Sobib");
		}

	}

}
