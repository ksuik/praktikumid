package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimeseN2ited {

	public static void main(String[] args) {
		
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		System.out.println("Sisesta nimi ja vanus");
		String nimi = TextIO.getlnString();
		int vanus = TextIO.getlnInt();
		Inimene keegi = new Inimene(nimi, vanus);
		inimesed.add(keegi);
//		inimened.add(new Inimene("Mati", 34));
		
		for(Inimene inimene : inimesed){
			System.out.println(inimene);
		}

	}

}
