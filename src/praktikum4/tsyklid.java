package praktikum4;

public class tsyklid {

	public static void main(String[] args) {
			
		int tabelisuurus = 7;
		
		for (int i = 0; i < tabelisuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();
		
		for(int i = 0; i < tabelisuurus; i++) {
			System.out.print("| ");
			for (int j = 0 ; j < tabelisuurus; j++) {
				if ( i == j || i + j == tabelisuurus - 1)
					System.out.print("x ");
				 else 
					 System.out.print("0 ");
			}
			
			System.out.println("|");
		}
		
		for(int i = 0; i < tabelisuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();
	}
}
