package praktikumid14;

public class Joon {
	
	Punkt algus, l6pp;
	
	public Joon(Punkt p1, Punkt p2){
		algus = p1;
		l6pp = p2;
	}
	@Override
	public String toString() {
		return "Joon(" + algus + ", " + l6pp +")";
	}
	public double pikkus() {
		double a = algus.y - l6pp.y;
		double b = algus.x - l6pp.x;
		double c = Math.sqrt(Math.pow(a, 2)+ Math.pow(b, 2));
		return c;
	}

}
