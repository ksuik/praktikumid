package praktikumid14;

public class Silinder extends Ring{

	double k6rgus;

	public Silinder(Ring r, double k6rgus) {
		super(r.keskpunkt, r.raadius);
		this.k6rgus = k6rgus;
	}
	@Override
	public String toString() {
		return "Silindri pindala: " + pindala() + ". Silindri ruumala on: " + ruumala();
	}
	
	public double pindala (){
		return  2*super.pindala() + super.ymberm66t() * k6rgus ;

	}
	public double ruumala() {
		return 2 * super.pindala() * k6rgus;
	}
}
