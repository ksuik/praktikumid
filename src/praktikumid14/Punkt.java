package praktikumid14;

public class Punkt {
	
	int x;
	int y;
	
	public Punkt() {
		
	}
	
	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override //anntotatsioon, kirjutab objekt klassis overide-i üle,väldib vigu
	public String toString() {
		
		return "Punkt(" + x + ", " + y + ")";//super.toString(); //super viitab ülemklassile ehk object klassile 
	}

}
