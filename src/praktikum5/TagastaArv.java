package praktikum5;

import lib.TextIO;

public class TagastaArv {
	
	public static void main(String[] argumendid) {
	
		int kasutajaSisestas = kasutajaSisestus(1, 10);
		System.out.println("kasutajaSisestus meetod tagastas:" + kasutajaSisestas);
	
	}
	
	public static int kasutajaSisestus(int min, int max) {
		while (true){
			System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
			int sisestus = TextIO.getlnInt();
			if (sisestus >= min && sisestus <= max) { 
				return sisestus;	
			} else {
				System.out.println("Vigane sisestus! Proovi uuesti.");
			}
		}
	}
	
}
