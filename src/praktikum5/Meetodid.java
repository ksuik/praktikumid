package praktikum5;

import praktikum1.TextIO;

public class Meetodid {

	public static void main(String[] argumendid) {

        int kasutajaSisestas = kasutajaSisestus1(1, 10);
        System.out.println("kasutajaSisestus meetod tagastas: " + kasutajaSisestas);
        
    }
    
    public static int kasutajaSisestus1(int min, int max) {
        while (true) {
            int sisestus = TextIO.getlnInt();
            if (sisestus >= min && sisestus <= max) {
                return sisestus;
            } else {
                System.out.println("Vigane sisestus! Proovi uuesti.");
            }
        }
    }
    


    public static int kasutajaSisestus(int min, int max) {

        int sisestus;
        do {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            sisestus = TextIO.getlnInt();           
        } while (sisestus < min || sisestus > max);
        return sisestus;
    }
    
}