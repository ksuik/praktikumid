package praktikum12;

public class T2isarvumassiiv {
	
	public static void main(String[] args){
//		int [] minuMassiiv = {2, 4, 7, 8};
//		tryki(minuMassiiv);
		
		int [][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		tryki(neo);
//		tryki(ridadeSummad(neo));
//		System.out.println(korvalDiagonaaliSumma(neo));
		tryki(ridadeMaksimumid(neo));
		
	}
	public static int miinimum(int[][] maatriks){
		int seniV2ikseim = Integer.MAX_VALUE;
		for (int[] rida : maatriks) {
			for(int number : rida){
				if(number < seniV2ikseim){
					seniV2ikseim = number;
				}
			}
			
		}
		return seniV2ikseim;
	}
	public static int[] ridadeMaksimumid(int[][] maatriks){
		int[] maksimumid = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			maksimumid[i] = reaMaksimum(maatriks[i]);
		}
		return maksimumid;
		
	}
	public static int reaMaksimum(int[] rida){
		int maksimum = rida[0];
		for (int i = 0; i < rida.length; i++) {
			if(rida[i] > maksimum){
				maksimum = rida[i];
			}
				
		}
		return maksimum;
	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {

		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i + j == maatriks.length - 1) {
					summa += maatriks[i][j];
				}
			}
		}
		return summa;

	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}
		return summad;
	}

	public static int reaSumma(int[] rida) {
		int summa = 0;
		for (int i : rida) {
			summa += i;
		}
		return summa;
	}

	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}
		System.out.println();
	}

	public static void tryki(int[][] maatriks) {
//		System.out.println("Kahemõõtmeline massiiv ehk maatriks");
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]);
		}

	}

}
